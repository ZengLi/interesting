private $i = 0; // 结果的指针
    private $res = [];

    /**
     * 入口
     * @auther 浔无涯
     * @email zl939144892@icloud.com
     * @date 2018/7/11 14:36
     * @param int $nTarget 目标值
     * @param array $aSource 给定的数组
     * @return array
     */
    public function main($nTarget = 0, $aSource = [])
    {
      sort($aSource); // 对数组进行排序
        foreach ($aSource as $k => $v) {
            // 第一遍的时候只有一个值
            if ($v == $nTarget) { // 单个值就符合条件
                array_push($this->res, [$v]);
                break; //  没必要继续进行了
            } elseif ($v > $nTarget) break;

            unset($aSource[$k]); // 从源数组中删除当前值, 直接在源数组上操作, 以节省资源, 提高速度 (之前遍历过, 没必要保留了)
            $this->res = $this->addFind($nTarget, array_values($aSource), $v, [$v]); // array_values() 用来重新排列unset打乱的键
        }
        return $this->res;
    }

    /**
     * 进行递归处理
     * @auther 浔无涯
     * @email zl939144892@icloud.com
     * @date 2018/7/11 17:01
     * @param int $Ttarget 目标值
     * @param array $aSource 给定的数组
     * @param array $res 之前的结果集
     * @param int $nTmpAddRes 上一次运算获得的值
     * @param array $aTmpRes 参与上一次运算的值
     * @return array
     */
    private function addFind($Ttarget, $aSource = [], $nTmpAddRes = 0, $aTmpRes = [])
    {
        foreach ($aSource as $k => $v) {
            $nTmp = $v + $nTmpAddRes; // 计算结果
            if ($nTmp > $Ttarget) {
                break; // 冒了
            } elseif ($nTmp == $Ttarget) { // 获得一个结果
                array_push($aTmpRes, $v); // 把当前符合要求的值与上一次参与运算的合并
                sort($aTmpRes); // 对数组进行一下排序, 可以不要
                $this->res[$this->i++] = $aTmpRes; // 压入结果集
                break;
            } else { // 不达结果, 继续递归
                $aTmpRes2 = $aTmpRes; // 防止影响下一次的计算
                array_push($aTmpRes2, $v); // 把当前的值与上一次参与运算的合并
                unset($aSource[$k]); // 从源中删除
                $this->res = $this->addFind($Ttarget, array_values($aSource), $nTmp, $aTmpRes2);
            }
        }
        return $this->res;
    }